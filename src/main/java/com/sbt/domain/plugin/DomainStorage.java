package com.sbt.domain.plugin;

import com.intellij.psi.PsiClass;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DomainStorage {
    private DomainStorage() {
    }

    private static DomainStorage instance = new DomainStorage();

    public static DomainStorage getInstance() {
        return instance;
    }

    private Map<String, DomainNode> domainNodeMap = new HashMap<>();
    private Map<String, DomainNode> shortNameToDomainNode = new HashMap<>();


    public Map<String, DomainNode> getDomainNodeMap() {
        return domainNodeMap;
    }

    public DomainNode getByFullName(String fullName) {
        return domainNodeMap.get(fullName);
    }

    public void linkHierarchy() {
        for (Map.Entry<String, DomainNode> entry : domainNodeMap.entrySet()) {
            DomainNode node = entry.getValue();
            if (node.getPsi() == null || node.isProperty())
                continue;
            PsiClass psi = (PsiClass) node.getPsi();
            if (psi.getSuperClass() == null)
                continue;
            PsiClass superClass = psi.getSuperClass();
            String fullName = PsiHelper.fetchPackageNameForClass(superClass) + "." + superClass.getName();
            DomainNode superDomainNode = domainNodeMap.get(fullName);
            if (superDomainNode == null) {
                System.out.println("non domain super class " + fullName);
                continue;
            }
            superDomainNode.getChildren().add(node);
            node.setParent(superDomainNode);
        }
    }

    public Collection<DomainNode> getAllDomainNodes() {
        return domainNodeMap.values();
    }

    public void put(DomainNode node) {
        domainNodeMap.put(node.getFullName(), node);
        shortNameToDomainNode.put(node.getName(), node);
    }

    public boolean containsFullName(String fullName) {
        return domainNodeMap.containsKey(fullName);
    }

    public Collection<String> getAllClassesNames() {
        return domainNodeMap.keySet();
    }

}
