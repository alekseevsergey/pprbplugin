package com.sbt.domain.plugin;

import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.DataContext;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.source.PsiClassImpl;

public class AddClassAction extends AnAction {
    @Override
    public void actionPerformed(AnActionEvent anActionEvent) {
        DataContext dataContext = anActionEvent.getDataContext();
        Editor editor = dataContext.getData(CommonDataKeys.EDITOR);
        Project data = dataContext.getData(CommonDataKeys.PROJECT);
        PsiFile psiFile = dataContext.getData(CommonDataKeys.PSI_FILE);

        for (PsiElement element : psiFile.getChildren()){
            if (!(element instanceof PsiClassImpl))
                continue;
            PsiClassImpl psiClass = (PsiClassImpl) element;
            DomainClassesStorage.getInstance().addDomainClass(psiClass);
        }
    }
}
