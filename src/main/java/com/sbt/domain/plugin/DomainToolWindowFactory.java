package com.sbt.domain.plugin;

import com.intellij.openapi.editor.CaretModel;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.editor.ScrollType;
import com.intellij.openapi.fileEditor.FileEditor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.TextEditor;
import com.intellij.openapi.project.DumbService;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowFactory;
import com.intellij.psi.*;
import com.intellij.psi.impl.source.PsiClassReferenceType;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.search.searches.ClassInheritorsSearch;
import com.intellij.psi.search.searches.ReferencesSearch;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.ui.content.Content;
import com.intellij.ui.content.ContentFactory;
import com.sbt.domain.plugin.dialog.DomainDialog;
import com.sbt.domain.plugin.dialog.DtoDialog;
import com.sbt.domain.plugin.dialog.NewClassDialog;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class DomainToolWindowFactory implements ToolWindowFactory {
    private ToolWindow myToolWindow;
    private JPanel panel;
    private JTree tree;
    private JRadioButton code;
    private JRadioButton description;
    private JButton newClass;
    private JList propertiesList;
    private JButton dtoButton;
    private DefaultMutableTreeNode root;
    private Project project;
    private static DomainToolWindowFactory instance;
    private DomainStorage storage = DomainStorage.getInstance();

    public DomainToolWindowFactory() {
        instance = this;
        code.addChangeListener(e -> {
            DomainNode.setCodeFirst(code.getModel().isSelected());
            tree.updateUI();
        });
        newClass.addActionListener(e -> new NewClassDialog(project).show());
        dtoButton.addActionListener(e -> showAddDtoDialog());
    }

    private void showAddDtoDialog() {
        if (tree.getSelectionPaths() == null)
            return;
        TreePath treePath = tree.getSelectionPaths()[0];
        DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) treePath.getLastPathComponent();
        DomainNode selected = (DomainNode) treeNode.getUserObject();
        if (selected.isProperty())
            selected = selected.getParent();

        DtoDialog dialog = new DtoDialog(selected);
        dialog.show();
    }


    public void createToolWindowContent(@NotNull Project project, @NotNull ToolWindow toolWindow) {
        myToolWindow = toolWindow;
        this.project = project;
        DomainClassesStorage.getInstance().setToolbar(this);

        ContentFactory contentFactory = ContentFactory.SERVICE.getInstance();
        Content content = contentFactory.createContent(panel, "", false);
        toolWindow.getContentManager().addContent(content);

        root = new DefaultMutableTreeNode("domain");
        TreeModel model = new DefaultTreeModel(root);
        tree.setModel(model);

        treeClickListener();
        treeSeelctListener();
        //listClickListener();

        DumbService.getInstance(project).runWhenSmart(() -> initDomain(project));

    }

//    private void listClickListener() {
//        propertiesList.addMouseListener(new MouseListener() {
//            public void mouseClicked(MouseEvent e) {
//                boolean ctrl = (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0;
//           } else
//                if (e.getClickCount() >= 2 || ctrl) {
//                    handleListClick(e, ctrl);
//                }
//            }
//
//            @Override
//            public void mousePressed(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseReleased(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseEntered(MouseEvent e) {
//
//            }
//
//            @Override
//            public void mouseExited(MouseEvent e) {
//
//            }
//        });
//    }

    private void handleListClick(MouseEvent e, boolean navigatePropType) {
        navigateToNode(getListItemClicked(e));
    }

    private void treeSeelctListener() {
        tree.addTreeSelectionListener(new TreeSelectionListener() {
            @Override
            public void valueChanged(TreeSelectionEvent e) {
                TreePath path = e.getPath();
                DefaultListModel model = new DefaultListModel();
                DefaultMutableTreeNode node = (DefaultMutableTreeNode) path.getLastPathComponent();
                DomainNode domainNode = (DomainNode) node.getUserObject();

                List<DomainNode> classesWithProperties = new LinkedList<>();
                classesWithProperties.add(domainNode);

                while (domainNode.getParent() != null) {
                    domainNode = domainNode.getParent();
                    if (domainNode.hasProperties())
                        classesWithProperties.add(0, domainNode);
                }

                for (DomainNode clazz : classesWithProperties) {
                    model.addElement(new ListModelWrapper(clazz));
                    for (DomainNode property : clazz.getChildren()) {
                        if (!property.isProperty())
                            continue;
                        model.addElement(new ListModelWrapper(property));
                    }
                }

                propertiesList.setModel(model);
            }
        });
    }

    private void treeClickListener() {
        tree.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                boolean ctrl = (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0;
                boolean alt = (e.getModifiersEx() & InputEvent.ALT_DOWN_MASK) != 0;

                if (alt) {
                    showDomainDialog(e);
                } else if (e.getClickCount() >= 2 || ctrl) {
                    handleTreeDClick(e, ctrl);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void showDomainDialog(MouseEvent e) {
        DefaultMutableTreeNode treeNode = getDefaultMutableTreeNodeClicked(e);
        if (treeNode == null) return;
        DomainNode domainNode = (DomainNode) treeNode.getUserObject();
        if (domainNode.isProperty())
            domainNode = domainNode.getParent();

        DomainDialog dialog = new DomainDialog(domainNode);
        dialog.show();
    }

    private void handleTreeDClick(MouseEvent e, boolean navigatePropType) {

        DefaultMutableTreeNode treeNode = getDefaultMutableTreeNodeClicked(e);
        if (treeNode == null) return;
        DomainNode domainNode = (DomainNode) treeNode.getUserObject();

        if (domainNode.getPsi() == null)
            return;

        if (domainNode.isProperty() && navigatePropType) {
            domainNode = getDomainNodeForPropertyType(domainNode);
        }

        navigateToNode(domainNode);
    }

    @NotNull
    private DomainNode getDomainNodeForPropertyType(DomainNode domainNode) {
        PsiField prop = (PsiField) domainNode.getPsi();
        if (prop.getType() instanceof PsiClassReferenceType) {
            PsiClassReferenceType refType = (PsiClassReferenceType) prop.getType();
            PsiJavaCodeReferenceElement reference = refType.getReference();

            DomainNode node = storage.getByFullName(reference.getQualifiedName());
            if (node != null) {
                domainNode = node;
            }
        }
        return domainNode;
    }

    private void navigateToNode(DomainNode domainNode) {
        PsiJavaFile parent = domainNode.isProperty()
                ? (PsiJavaFile) domainNode.getParent().getPsi().getParent()
                : (PsiJavaFile) domainNode.getPsi().getParent();
        FileEditorManager manager = FileEditorManager.getInstance(project);
        manager.openFile(parent.getVirtualFile(), true);
        if (domainNode.isProperty()) {
            FileEditor[] editors = manager.getEditors(parent.getVirtualFile());
            Editor editor = ((TextEditor) editors[0]).getEditor();
            CaretModel caretModel = editor.getCaretModel();
            PsiElement navigationElement = domainNode.getPsi().getNavigationElement();

            caretModel.moveToOffset(navigationElement.getTextOffset());
            editor.getScrollingModel().scrollToCaret(ScrollType.CENTER);
        }
    }

    @Nullable
    private DefaultMutableTreeNode getDefaultMutableTreeNodeClicked(MouseEvent e) {
        TreePath pathForLocation = tree.getPathForLocation(e.getX(), e.getY());
        DefaultMutableTreeNode lastPathComponent = (DefaultMutableTreeNode) pathForLocation.getLastPathComponent();
        if (!(lastPathComponent.getUserObject() instanceof DomainNode))
            return null;
        return lastPathComponent;
    }

    @Nullable
    private DomainNode getListItemClicked(MouseEvent e) {
        ListModelWrapper selectedValue = (ListModelWrapper) propertiesList.getSelectedValue();
        return selectedValue.getNode();
    }

    private void updateDomain(PsiTreeChangeEvent psiTreeChangeEvent) {
        PsiElement element = psiTreeChangeEvent.getElement();
        if (null == element)
            element = psiTreeChangeEvent.getChild();
        if (null == element)
            return;
        PsiClass psiClass = PsiTreeUtil.getParentOfType(element, PsiClass.class);
        if (psiClass == null)
            return;

        if (storage.containsFullName(psiClass.getQualifiedName())) {
            updateClassNode(storage.getByFullName(psiClass.getQualifiedName()), psiClass);
        }
    }

    // TODO handle tree change
    private void updateClassNode(DomainNode node, PsiClass psiClass) {
        node.setPsi(psiClass);
        node.getChildren().removeIf(DomainNode::isProperty);
        handleFieldsForClass(node);
    }

    private void initDomain(@NotNull Project project) {
        if (DumbService.getInstance(project).isDumb()) {
            System.out.println("ERROR initDomain in dumb mode");
        }

        PsiClass dplStore = JavaPsiFacade.getInstance(project).findClass("com.sber.pprbplugin.Main",
                GlobalSearchScope.allScope(project));

        preparePsiChangeListener(project);

        Collection<PsiReference> all = ReferencesSearch.search(dplStore).findAll();
        all.addAll(ReferencesSearch.search(dplStore).findAll());
        for (PsiReference reference : all) {
            if (reference.getElement().getParent() instanceof PsiImportStatement)
                continue;
            PsiElement domainClass = reference.getElement().getParent();
            if (domainClass instanceof PsiClass) {
                PsiClass domainPsiClass = (PsiClass) domainClass;
                handleDomainClass(domainPsiClass);
            }
            reference.getElement().getParent();
        }

        handleDomainClass(dplStore);

        storage.linkHierarchy();

        List<DomainNode> roots = new ArrayList<>();
        for (DomainNode node : storage.getAllDomainNodes()) {
            if (node.getPsi() == null)
                continue;
            if (node.getParent() == null) {
                System.out.println("root: " + node.getFullName() + " children " + node.getChildren().size());
                roots.add(node);
            }
        }

        buildTree(this.root, roots);

    }


    private void preparePsiChangeListener(@NotNull Project project) {
        PsiManager.getInstance(project).addPsiTreeChangeListener(new PsiTreeChangeListener() {
            @Override
            public void beforeChildAddition(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {
            }

            @Override
            public void beforeChildRemoval(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {

            }

            @Override
            public void beforeChildReplacement(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {

            }

            @Override
            public void beforeChildMovement(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {

            }

            @Override
            public void beforeChildrenChange(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {

            }

            @Override
            public void beforePropertyChange(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {

            }

            @Override
            public void childAdded(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {
                updateDomain(psiTreeChangeEvent);
            }

            @Override
            public void childRemoved(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {
                updateDomain(psiTreeChangeEvent);
            }

            @Override
            public void childReplaced(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {
                updateDomain(psiTreeChangeEvent);
            }

            @Override
            public void childrenChanged(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {
                updateDomain(psiTreeChangeEvent);
            }

            @Override
            public void childMoved(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {
                updateDomain(psiTreeChangeEvent);
            }

            @Override
            public void propertyChanged(@NotNull PsiTreeChangeEvent psiTreeChangeEvent) {
                updateDomain(psiTreeChangeEvent);
            }
        });
    }

    private void buildTree(DefaultMutableTreeNode root, List<DomainNode> roots) {
        for (DomainNode node : roots) {
            DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(node);
            root.add(treeNode);
            List<DomainNode> classes = new ArrayList<>();
            for (DomainNode child : node.getChildren()) {
                if (!child.isProperty()) {
                    classes.add(child);
                    continue;
                }
            }
            buildTree(treeNode, classes);
        }
    }

    private static PsiElement fetchAnnotation(PsiElement element, String name) {
        for (PsiElement child : element.getChildren()) {
            if (!(child instanceof PsiModifierList)) {
                continue;
            }
            for (PsiElement annotation : child.getChildren()) {
                if (!(annotation instanceof PsiAnnotation)) {
                    continue;
                }
                String className = annotation.getChildren()[1].getText();
                if (name.equals(className)) {
                    return annotation;
                }
            }

        }
        return null;
    }


    private void handleDomainClass(PsiClass domainPsiClass) {
        if (domainPsiClass instanceof PsiAnonymousClass)
            return;
        domainPsiClass.getParent();
        PsiElement parent = domainPsiClass.getParent();
        String packageFullName = PsiHelper.fetchPackageNameForClass(parent);
        String fullName = packageFullName + "." + domainPsiClass.getName();

        if (DomainStorage.getInstance().containsFullName(fullName))
            return;

        DefaultMutableTreeNode node = new DefaultMutableTreeNode(domainPsiClass.getName());
        DomainNode domainNode = new DomainNode(domainPsiClass.getName(), fullName, null);
        domainNode.setDplStoreTo(fetchDplStoreTo(domainPsiClass));
        domainNode.setTitle(fetchTitle(domainPsiClass));
        domainNode.setPsi(domainPsiClass);

        DomainStorage.getInstance().put(domainNode);

        PsiClass superClass = domainPsiClass.getSuperClass();
        if (superClass != null && !"Object".equals(superClass.getName()))
            handleDomainClass(superClass);

        Collection<PsiClass> all = ClassInheritorsSearch.search(domainPsiClass).findAll();
        for (PsiClass subClass : all)
            handleDomainClass(subClass);

        handleFieldsForClass(domainNode);

    }

    private void handleFieldsForClass(DomainNode domainNode) {
        PsiClass psi = (PsiClass) domainNode.getPsi();
        for (PsiElement field : psi.getFields()) {
            PsiElement property = fetchAnnotation(field, "Property");
            if (property == null)
                continue;
            PsiElement[] children = property.getChildren()[2].getChildren()[1].getChildren();
            String title = "";
            for (PsiElement element : children) {
                if (element instanceof PsiLiteralExpression)
                    title = element.getText();
            }
            title = stripQuotes(title);
            field.getText();
            PsiTypeElement typeElement = (PsiTypeElement) fetchChild(field, PsiTypeElement.class);
            String name = fetchChild(field, PsiIdentifier.class).getText();
            DomainNode propertyNode = new DomainNode(name, domainNode.getFullName() + "#" + name, null);
            propertyNode.setTitle(title);
            propertyNode.setPropertyType(typeElement.getText());
            propertyNode.setPropertyTypeFullName(typeElement.getType().getCanonicalText());
            propertyNode.setProperty(true);
            propertyNode.setParent(domainNode);
            propertyNode.setPsi(field);
            domainNode.getChildren().add(propertyNode);
        }
    }

    private static PsiElement fetchChild(PsiElement element, Class clazz) {
        for (PsiElement child : element.getChildren()) {
            if (clazz.isAssignableFrom(child.getClass()))
                return child;
        }
        return null;
    }

    private String fetchDplStoreTo(PsiClass domainPsiClass) {
        PsiElement entity = fetchAnnotation(domainPsiClass, "DplStore");
        if (entity != null) {

            for (PsiElement element : entity.getChildren()[2].getChildren()) {
                if (!(element instanceof PsiNameValuePair))
                    continue;
                PsiNameValuePair pair = (PsiNameValuePair) element;
                pair.getName();
                if ("value".equals(pair.getName()) || null == pair.getName())
                    return pair.getValue().getText();
            }
            return null;

        }
        return null;
    }

    @Nullable
    private String fetchTitle(PsiClass domainPsiClass) {
        PsiElement entity = fetchAnnotation(domainPsiClass, "Entity");
        if (entity != null) {
            return stripQuotes(entity.getChildren()[2].getChildren()[1].getChildren()[4].getText());
        }
        return null;
    }

    @Nullable
    private String stripQuotes(String text) {
        String[] split = text.split("\"");
        if (split.length == 2) {
            return split[1];
        } else {
            if (split.length == 0)
                return null;
            else
                throw new RuntimeException("text " + text);
        }
    }


    public void init(ToolWindow window) {

    }

}
