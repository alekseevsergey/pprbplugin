package com.sbt.domain.plugin;

public class ListModelWrapper {
    private DomainNode node;

    public ListModelWrapper(DomainNode node) {
        this.node = node;
    }

    public DomainNode getNode() {
        return node;
    }

    @Override
    public String toString() {
        return (node.isProperty() ? " (P)" : "(C) ") + node.getName();
    }
}
