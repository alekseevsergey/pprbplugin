package com.sbt.domain.plugin;

import com.intellij.psi.PsiElement;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.ArrayList;
import java.util.List;

public class DomainNode {
    private String name;
    private String fullName;
    private String title;
    private DefaultMutableTreeNode treeNode;
    private PsiElement psi;
    private DomainNode parent;
    private List<DomainNode> children = new ArrayList<>();
    private boolean property;
    private String propertyType;
    private String propertyTypeFullName;
    private String dplStoreTo;
    private static boolean codeFirst = true;

    public DomainNode(String name, String fullName, DefaultMutableTreeNode treeNode) {
        this.name = name;
        this.fullName = fullName;
        this.treeNode = treeNode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public DefaultMutableTreeNode getTreeNode() {
        return treeNode;
    }

    public void setTreeNode(DefaultMutableTreeNode treeNode) {
        this.treeNode = treeNode;
    }

    public PsiElement getPsi() {
        return psi;
    }

    public void setPsi(PsiElement psi) {
        this.psi = psi;
    }

    public DomainNode getParent() {
        return parent;
    }

    public void setParent(DomainNode parent) {
        this.parent = parent;
    }

    public List<DomainNode> getChildren() {
        return children;
    }

    public void setChildren(List<DomainNode> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return codeFirst
                ?
                (property
                        ? "#" + name + ":" + propertyType + " - " + title
                        : (title == null ? name : name + " - " + title
                        + (dplStoreTo != null ? " (->" + dplStoreTo + ")" : "")))
                :
                (property
                        ? title + " - " + "#" + name + ":" + propertyType
                        : (title == null ? name : title + " - " + name
                        + (dplStoreTo != null ? " (->" + dplStoreTo + ")" : ""))
                );
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isProperty() {
        return property;
    }

    public void setProperty(boolean property) {
        this.property = property;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public String getPropertyTypeFullName() {
        return propertyTypeFullName;
    }

    public void setPropertyTypeFullName(String propertyTypeFullName) {
        this.propertyTypeFullName = propertyTypeFullName;
    }

    public String getDplStoreTo() {
        return dplStoreTo;
    }

    public void setDplStoreTo(String dplStoreTo) {
        this.dplStoreTo = dplStoreTo;
    }

    public static boolean isCodeFirst() {
        return codeFirst;
    }

    public static void setCodeFirst(boolean codeFirst) {
        DomainNode.codeFirst = codeFirst;
    }

    public boolean hasProperties() {
        for (DomainNode node : children) {
            if (node.isProperty())
                return true;
        }
        return false;
    }
}