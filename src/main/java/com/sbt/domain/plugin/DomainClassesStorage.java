package com.sbt.domain.plugin;

import com.intellij.psi.PsiAnnotation;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiModifierList;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class DomainClassesStorage {
    private Map<String, PsiClass> classes = new HashMap<>();
    private DomainToolWindowFactory toolbar;

    private DomainClassesStorage() {
    }

    private static DomainClassesStorage instance = new DomainClassesStorage();

    public static DomainClassesStorage getInstance() {
        return instance;
    }

    public void addDomainClass(PsiClass clazz) {
        addDomainClassInternal(clazz);
    }

    public void addDomainClasses(Collection<PsiClass> psiClasses) {
        for (PsiClass psiClass : psiClasses)
            classes.put(psiClass.getName(), psiClass);
    }

    private void addDomainClassInternal(PsiClass clazz) {
        PsiElement modiferList = null;
        for (PsiElement element : clazz.getChildren()) {
            if (element instanceof PsiModifierList) {
                modiferList = element;
                break;
            }
        }

        for (PsiElement element : modiferList.getChildren()) {
            if (!(element instanceof PsiAnnotation))
                continue;
            PsiAnnotation annotation = (PsiAnnotation) element;
            if ("com.sbt.kmd.mapper.api.annotations.DplStore".equals(annotation.getQualifiedName())) {
                classes.put(clazz.getName(), clazz);
            }
        }
    }


    public Collection<PsiClass> listClasses() {
        return classes.values();
    }

    public DomainToolWindowFactory getToolbar() {
        return toolbar;
    }

    public void setToolbar(DomainToolWindowFactory toolbar) {
        this.toolbar = toolbar;
    }
}
