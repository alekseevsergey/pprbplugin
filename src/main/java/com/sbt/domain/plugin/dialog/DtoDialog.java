package com.sbt.domain.plugin.dialog;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.PackageIndex;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.LabeledComponent;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.ui.components.panels.VerticalLayout;
import com.sbt.domain.plugin.DomainNode;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

public class DtoDialog extends DialogWrapper {

    private JPanel main;
    private DomainNode node;
    private JTree domainTree;
    private JTree dtoTree;
    private JTextField dtoPackage;
    private JTextField dtoName;
    private List<DomainNode> dtoProperties = new ArrayList<>();
    private Project project;
    private JavaCodeStyleManager manager;
    private PsiElementFactory factory;


    public DtoDialog(DomainNode node) {
        super(node.getPsi().getProject());
        project = node.getPsi().getProject();

        this.node = node;
        setTitle("dto");
        main = new JPanel();
        domainTree = new JTree();
        dtoTree = new JTree();

        JPanel dtoConfig = createConfigPanel();

        main.add(domainTree);
        main.add(dtoConfig);
        main.add(dtoTree);

        DefaultMutableTreeNode domainRoot = new DefaultMutableTreeNode(node);
        for (DomainNode property : node.getChildren()) {
            if (!property.isProperty())
                continue;
            domainRoot.add(new DefaultMutableTreeNode(property));
        }
        DefaultTreeModel treeModel = new DefaultTreeModel(domainRoot);
        domainTree.setModel(treeModel);
        domainTree.updateUI();

        updateDtoTree();

        domainTreeMouseListener();

        init();
    }

    @NotNull
    private JPanel createConfigPanel() {
        JPanel dtoConfig = new JPanel();
        dtoConfig.setLayout(new VerticalLayout(0));
        dtoPackage = new JTextField();
        dtoPackage.setText(fetchPackageName());
        dtoConfig.add(LabeledComponent.create(dtoPackage, "package"));
        dtoName = new JTextField();
        dtoName.setText(node.getName());
        dtoConfig.add(LabeledComponent.create(dtoName, "name"));
        JButton generate = new JButton();
        generate.setText("Generate");
        dtoConfig.add(generate);
        generate.addActionListener(e -> generateDto());
        return dtoConfig;
    }

    @NotNull
    private String fetchPackageName() {
        String qualifiedName = getNodeClassFullName();
        return qualifiedName.substring(0, qualifiedName.lastIndexOf("."));
    }

    private void generateDto() {
        VirtualFile[] directoriesByPackageName =
                PackageIndex.getInstance(project)
                        .getDirectoriesByPackageName(dtoPackage.getText(), true);
        PsiDirectory directory = PsiManager.getInstance(project)
                .findDirectory(directoriesByPackageName[0]);
        PsiClass dtoClass = JavaDirectoryService.getInstance().createClass(directory, dtoName.getText() + "DTO");
        PsiClass converterClass = JavaDirectoryService.getInstance().createClass(directory,
                dtoName.getText() + "Converter");
        new WriteCommandAction.Simple(project, dtoClass.getContainingFile()) {
            @Override
            protected void run() throws Throwable {
                writeDto(dtoClass);
                writeConverter(converterClass, dtoClass);
            }
        }.execute();

    }

    private void writeDto(PsiClass dtoClass) {
        manager = JavaCodeStyleManager.getInstance(dtoClass.getProject());
        factory = JavaPsiFacade.getInstance(dtoClass.getProject()).getElementFactory();

        for (DomainNode property : dtoProperties) {
            manager.shortenClassReferences(
                    dtoClass.add(
                            factory.createFieldFromText(
                                    "private " + property.getPropertyTypeFullName() + " " + property.getName() + ";",
                                    dtoClass)));
        }

        createAnnotation(dtoClass, "@lombok.Data");
        createAnnotation(dtoClass, "@lombok.NoArgsConstructor");

        //        @Data
        //        @NoArgsConstructor
    }

    private void writeConverter(PsiClass converterClass, PsiClass dtoClass) {
        PsiClass classFromText = factory.createClassFromText("public class " + dtoName.getText() + "Converter implements " +
                "com.sbt.bm.loans4b.service.mapper.dto.DtoConverter<" +
                getNodeClassFullName() + ", " + dtoClass.getQualifiedName()
                + "> {\n\n}", null);
        converterClass = (PsiClass) converterClass.replace(
                PsiTreeUtil.findChildOfType(classFromText, PsiClass.class));
        manager.shortenClassReferences(converterClass);

        manager.shortenClassReferences(
                converterClass.add(
                        factory.createMethodFromText(
                                "@Override\n" + "public " + dtoClass.getQualifiedName() + " convert("
                                        + getNodeClassFullName() + " entity){\n" +
                                        dtoClass.getQualifiedName() + " dto = new " + dtoClass.getQualifiedName() + "();\n" +
                                        passPropertiesToDto() +
                                        "return dto;\n" +
                                        "}"
                                , converterClass)));

        manager.shortenClassReferences(
                converterClass.add(
                        factory.createMethodFromText(
                                "@Override\n" + "public void fill("
                                        + dtoClass.getQualifiedName() + " dto, "
                                        + getNodeClassFullName() + " entity){\n" +
                                        passPropertiesToEntity() +
                                        "}"
                                , converterClass)));
        /*
            @Override
            public void fill(EpkOrganizationDTO to, EpkOrganization from) {

            }



        *     @Override
    public TestDTO convert(EpkOrganization entity) {
        return null;
    }

        * */
    }

    private String passPropertiesToDto() {
        StringBuffer buffer = new StringBuffer();
        for (DomainNode node : dtoProperties) {
            String name = DomainDialog.toCamelCase(node.getName());
            buffer.append("dto.set" + name + "(entity.get" + name + "());\n");
        }
        return buffer.toString();
    }

    private String passPropertiesToEntity() {
        StringBuffer buffer = new StringBuffer();
        for (DomainNode node : dtoProperties) {
            String name = DomainDialog.toCamelCase(node.getName());
            buffer.append("entity.set" + name + "(dto.get" + name + "());\n");
        }
        return buffer.toString();
    }

    private String getNodeClassFullName() {
        return ((PsiClass) node.getPsi()).getQualifiedName();
    }

    private void createAnnotation(PsiClass aClass, String annotationText) {
        manager.shortenClassReferences(
                aClass.addBefore(factory.createAnnotationFromText(annotationText, aClass), aClass));
    }


    // public class PriceEquivalentDto

    private void updateDtoTree() {
        DefaultMutableTreeNode domainRoot = new DefaultMutableTreeNode(dtoName.getText() + "Dto");
        DefaultTreeModel treeModel = new DefaultTreeModel(domainRoot);
        for (DomainNode property : dtoProperties) {
            domainRoot.add(new DefaultMutableTreeNode(property));
        }
        dtoTree.setModel(treeModel);
        dtoTree.updateUI();
    }

    private void domainTreeMouseListener() {
        domainTree.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
//                boolean ctrl = (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0;
//                boolean alt = (e.getModifiersEx() & InputEvent.ALT_DOWN_MASK) != 0;

//                if (alt) {
//                    showDomainDialog(e);
//                } else
                if (e.getClickCount() >= 2) {
                    addPropertytoDto();
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {

            }

            @Override
            public void mouseReleased(MouseEvent e) {

            }

            @Override
            public void mouseEntered(MouseEvent e) {

            }

            @Override
            public void mouseExited(MouseEvent e) {

            }
        });
    }

    private void addPropertytoDto() {
        DefaultMutableTreeNode treeModel = (DefaultMutableTreeNode) domainTree.getLastSelectedPathComponent();
        DomainNode property = (DomainNode) treeModel.getUserObject();
        if (!property.isProperty())
            return;
        if (dtoProperties.contains(property))
            return;
        dtoProperties.add(property);
        updateDtoTree();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return new JScrollPane(main);
    }
}
