package com.sbt.domain.plugin.dialog;

import com.intellij.ide.util.TreeClassChooser;
import com.intellij.ide.util.TreeClassChooserFactory;
import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.fileEditor.TextEditor;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.LabeledComponent;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.refactoring.RefactoringFactory;
import com.intellij.refactoring.RenameRefactoring;
import com.intellij.refactoring.SafeDeleteRefactoring;
import com.intellij.ui.TextFieldWithAutoCompletion;
import com.intellij.usageView.UsageInfo;
import com.sbt.domain.plugin.DomainNode;
import com.sbt.domain.plugin.DomainTypeComplitionProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class DomainDialog extends DialogWrapper {
    private DomainNode domainNode;
    private JPanel main;

    // private final JTextField newPropertyType;
    private TextFieldWithAutoCompletion<String> newPropertyType;

    private JTextField newPropertyName;
    private JTextField newPropertyTitle;


    public DomainDialog(DomainNode domainNode) {
        // TODO PSI
        super(domainNode.getPsi().getProject());

        this.domainNode = domainNode;
        setTitle("domain");
        main = new JPanel();
        JPanel propertiesPanel = new JPanel();
        main.add(propertiesPanel);
        //LayoutManager

        propertiesPanel.setLayout(new GridLayout(0, 4));
        for (DomainNode child : domainNode.getChildren()) {
            if (!child.isProperty())
                continue;
            preparePropertyPanel(propertiesPanel, child);
        }
        //JPanel propertyAddPanel = new JPanel();
        //propertiesPanel.add(propertyAddPanel);

        // TODO try PsiClass
        newPropertyType = TextFieldWithAutoCompletion.create(domainNode.getPsi().getProject(), Collections.EMPTY_LIST, true, "");
        newPropertyType.installProvider(new DomainTypeComplitionProvider());

        newPropertyType.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                boolean ctrl = (e.getModifiersEx() & InputEvent.CTRL_DOWN_MASK) != 0;
//                boolean alt = (e.getModifiersEx() & InputEvent.ALT_DOWN_MASK) != 0;
                if (' ' == e.getKeyChar() && ctrl) {
                    sectNewPropertyType();
                }

            }

            @Override
            public void keyPressed(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });
        newPropertyName = new JTextField();
        newPropertyTitle = new JTextField();
        JButton newPropertyAdd = new JButton();

        newPropertyAdd.addActionListener(e -> new WriteCommandAction.Simple(
                domainNode.getPsi().getProject(), domainNode.getPsi().getContainingFile()) {
            @Override
            protected void run() throws Throwable {
                addNewProperty();
            }
        }.execute());

        newPropertyAdd.setText("add");

        propertiesPanel.add(new Label("type"));
        propertiesPanel.add(new Label("name"));
        propertiesPanel.add(new Label("title"));
        propertiesPanel.add(new Label(""));

        propertiesPanel.add(newPropertyType);
        propertiesPanel.add(newPropertyName);
        propertiesPanel.add(newPropertyTitle);
        propertiesPanel.add(newPropertyAdd);

        init();
    }

    private void sectNewPropertyType() {
        TreeClassChooserFactory factory = TreeClassChooserFactory.getInstance(domainNode.getPsi().getProject());
        TreeClassChooser projectScopeChooser = factory.createProjectScopeChooser("type");
        projectScopeChooser.showDialog();
        PsiClass selected = projectScopeChooser.getSelected();
        newPropertyType.setText(selected.getQualifiedName());
    }

    private void addNewProperty() {
        PsiElement clazz = domainNode.getPsi();
        PsiElementFactory factory = JavaPsiFacade.getInstance(
                clazz.getProject()).getElementFactory();

//            PsiAnnotation annotationFromText = JavaPsiFacade.getInstance(psi.getProject()).getElementFactory()
//                    .createAnnotationFromText("@Property(newPropertyName = \"" + title + "\")", modifierList);


        String name = newPropertyName.getText();
        String type = newPropertyType.getText();
        String fieldCode = "@Property(name=\"" + newPropertyTitle.getText() + "\")\n" +
                "private " + type
                + " " + name + ";";
        JavaCodeStyleManager manager = JavaCodeStyleManager.getInstance(clazz.getProject());

        manager.shortenClassReferences(
                clazz.add(
                        factory.createFieldFromText(fieldCode, clazz)));


        manager.shortenClassReferences(
                clazz.add(
                        factory.createMethodFromText(
                                createGetterText(name, type),
                                clazz)));

        manager.shortenClassReferences(
                clazz.add(
                        factory.createMethodFromText(
                                createSetterText(name, type), clazz)));

    }

    public static String createSetterText(String name, String type) {
        return "public void set" + toCamelCase(name) + "(" + type + " " + name + ") {\n"
                + "this." + name + " = " + name + ";\n"
                + "}";
    }

    public static String createGetterText(String name, String type) {
        return "public " + type + " get" + toCamelCase(name) + "() {\n"
                + "return " + name + ";\n"
                + "}";
    }

    @NotNull
    public static String toCamelCase(String name) {
        return name.substring(0, 1).toUpperCase() + name.substring(1);
    }

    private void preparePropertyPanel(JPanel propertiesPanel, DomainNode child) {
        //JPanel propertyPanel = new JPanel();

        propertiesPanel.add(prepareType(child));
        propertiesPanel.add(prepareName(child));
        propertiesPanel.add(prepareTitleField(child));
        propertiesPanel.add(prepareDeleteButton(child));
        //propertiesPanel.add(propertyPanel, child.getName());
    }


    @NotNull
    private JButton prepareDeleteButton(DomainNode node) {
        JButton delete = new JButton();
        delete.setText("D");
        PsiField psiField = (PsiField) node.getPsi();
        delete.addActionListener(e -> new WriteCommandAction.Simple(node.getPsi().getProject(), node.getPsi().getContainingFile()) {
            @Override
            protected void run() throws Throwable {
                deleteOrDeprecateField(psiField, node);
            }
        }.execute());
        return delete;
    }

    private void deleteOrDeprecateField(PsiField psiField, DomainNode node) {
        RefactoringFactory factory = RefactoringFactory.getInstance(psiField.getProject());
        SafeDeleteRefactoring safeDelete = factory.createSafeDelete(new PsiElement[]{node.getPsi()});
        UsageInfo[] usages = safeDelete.findUsages();
        Collection<PsiElement> elementsToDelete = new ArrayList<>();
        elementsToDelete.add(node.getPsi());
        if (checkDeletePossible(psiField.getName(), usages, factory, elementsToDelete)) {
            doDelete(usages, elementsToDelete, factory);
        } else {
            depricateField(elementsToDelete);
        }
    }

    private void depricateField(Collection<PsiElement> elementsToDelete) {
        PsiElementFactory factory = JavaPsiFacade.getInstance(
                elementsToDelete.iterator().next().getProject())
                .getElementFactory();
        for (PsiElement element : elementsToDelete) {
            if (element instanceof PsiField) {
                PsiField deprecated = factory.createFieldFromText("@Deprecated()\n" + element.getText(), element.getParent());
                element.replace(deprecated);
            }
            if (element instanceof PsiMethod) {
                PsiMethod deprecated = factory.createMethodFromText("@Deprecated()\n" + element.getText(), element.getParent());
                element.replace(deprecated);
            }
        }
    }

    private void doDelete(UsageInfo[] usages, Collection<PsiElement> elementsToDelete, RefactoringFactory factory) {
        SafeDeleteRefactoring safeDelete = factory.createSafeDelete(elementsToDelete.toArray(
                new PsiElement[elementsToDelete.size()]));
        safeDelete.doRefactoring(safeDelete.findUsages());
    }

    private boolean checkDeletePossible(String oldName, UsageInfo[] usages, RefactoringFactory factory,
                                        Collection<PsiElement> elementsToDelete) {
        boolean result = true;
        for (UsageInfo usageInfo : usages) {
            usageInfo.getElement();
            PsiMethod method = PsiTreeUtil.getParentOfType(usageInfo.getElement(), PsiMethod.class);
            if (method.isConstructor()) {
                PsiParameter parameter = findParameterByName(oldName, method);
                if (null != parameter) {
                    elementsToDelete.add(parameter);
                }
                elementsToDelete.add(usageInfo.getElement());
                continue;
            }
            if (method.getName().startsWith("get") || method.getName().startsWith("set") || method.getName().startsWith("is")) {
                SafeDeleteRefactoring safeDelete = factory.createSafeDelete(new PsiElement[]{method});
                UsageInfo[] accessorUsages = safeDelete.findUsages();
                if (accessorUsages.length > 0)
                    result = false;
                elementsToDelete.add(method);
            } else {
                result = false;
            }
        }
        return result;
    }

    @NotNull
    private JTextField prepareType(DomainNode node) {
        JTextField field = new JTextField();
        field.setText(node.getPropertyType());
        field.setEnabled(false);
        field.setSize(50, 15);
        return field;
    }

    @NotNull
    private JTextField prepareName(DomainNode node) {
        JTextField field = new JTextField();
        field.setText(node.getName());
        PsiField psiField = (PsiField) node.getPsi();
        field.setSize(50, 15);
        field.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {

            }

            @Override
            public void focusLost(FocusEvent e) {
                new WriteCommandAction.Simple(node.getPsi().getProject(), node.getPsi().getContainingFile()) {
                    @Override
                    protected void run() throws Throwable {
                        renameField(field, node);
                    }
                }.execute();
            }
        });
        return field;
    }

    private void renameField(JTextField field, DomainNode node) {
        String newName = field.getText();
        PsiField psiField = (PsiField) node.getPsi();
        String oldName = psiField.getName();
        RefactoringFactory factory = RefactoringFactory.getInstance(psiField.getProject());

        RenameRefactoring fieldRename = factory.createRename(psiField, newName);
        UsageInfo[] usages = fieldRename.findUsages();
        if (usages.length == 0) {
            usages = fieldRename.findUsages();
        }
        for (UsageInfo usageInfo : usages) {
            PsiMethod method = PsiTreeUtil.getParentOfType(usageInfo.getElement(), PsiMethod.class);
            PsiParameter parameter = findParameterByName(oldName, method);
            if (parameter != null) {
                fieldRename.addElement(parameter, newName);
            }
            for (String prefix : new String[]{"get", "set", "is"}) {
                if (method.getName().equalsIgnoreCase(prefix + oldName)) {
                    fieldRename.addElement(method,
                            prefix + newName.substring(0, 1).toUpperCase() + newName.substring(1));
                }
            }
        }
        fieldRename.doRefactoring(fieldRename.findUsages());
    }

    @Nullable
    private PsiParameter findParameterByName(String oldName, PsiMethod method) {
        PsiParameter found = null;
        for (PsiParameter parameter : method.getParameterList().getParameters()) {
            if (parameter.getName().equals(oldName)) {
                found = parameter;
            }
        }
        return found;
    }

    @NotNull
    private JTextField prepareTitleField(DomainNode child) {
        JTextField field = new JTextField();
        field.setText(child.getTitle());
        field.setSize(50, 15);
        field.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                updatePropertyAnnotation(child, field.getText());
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                updatePropertyAnnotation(child, field.getText());
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                updatePropertyAnnotation(child, field.getText());
            }
        });
        return field;
    }

    protected void updatePropertyAnnotation(DomainNode node, String title) {
        node.setTitle(title);
        new WriteCommandAction.Simple(node.getPsi().getProject(), node.getPsi().getContainingFile()) {
            @Override
            protected void run() throws Throwable {
                updateAnnotation((PsiField) node.getPsi(), title);
            }
        }.execute();
    }

    private void updateAnnotation(PsiField psi, String title) {
        for (PsiElement element : psi.getChildren()) {
            if (!(element instanceof PsiModifierList))
                continue;
            PsiModifierList modifierList = (PsiModifierList) element;
            for (PsiElement modifer : modifierList.getChildren()) {
                if (!(modifer instanceof PsiAnnotation))
                    continue;
                PsiAnnotation annotation = (PsiAnnotation) modifer;
                if (!"com.sbt.bm.loans4b.audit.api.Property".equals(annotation.getQualifiedName()))
                    continue;
                for (PsiElement annotationElement : annotation.getChildren()) {
                    if (!(annotationElement instanceof PsiAnnotationParameterList))
                        continue;
                    PsiAnnotationParameterList parameterList = (PsiAnnotationParameterList) annotationElement;
                    for (PsiElement paramListElemet : parameterList.getChildren()) {
                        if (!(paramListElemet instanceof PsiNameValuePair))
                            continue;
                        PsiNameValuePair nameValuePair = (PsiNameValuePair) paramListElemet;
                        if (!"name".equals(nameValuePair.getName()))
                            continue;
                        PsiAnnotationMemberValue value = nameValuePair.getValue();
                        PsiAnnotation annotationFromText = JavaPsiFacade.getInstance(psi.getProject()).getElementFactory()
                                .createAnnotationFromText("@Property(name = \"" + title + "\")", modifierList);
                        annotation.replace(annotationFromText);

                        //nameValuePair.setValue(new PsiLiteralExpressionImpl(""));
//                                fetchDocument(psi).replaceString(
//                                        value.getTextOffset(), value.getTextOffset() + value.getTextLength(),
//                                        "\"" + title + "\"");
                        //modifierList.delete();
                    }
                }
            }
        }
    }

    @NotNull
    private Document fetchDocument(PsiField psi) {
        FileEditorManager manager = FileEditorManager.getInstance(psi.getProject());
        manager.openFile(psi.getContainingFile().getVirtualFile(), true);
        Editor editor = ((TextEditor) manager.getEditors(psi.getContainingFile().getVirtualFile())
                [0]).getEditor();
        return editor.getDocument();
    }

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return LabeledComponent.create(main, domainNode.getName());

    }
}
