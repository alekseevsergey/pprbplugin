package com.sbt.domain.plugin.dialog;

import com.intellij.openapi.command.WriteCommandAction;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.PackageIndex;
import com.intellij.openapi.ui.DialogWrapper;
import com.intellij.openapi.ui.LabeledComponent;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.codeStyle.JavaCodeStyleManager;
import com.intellij.ui.TextFieldWithAutoCompletion;
import com.intellij.ui.components.panels.VerticalLayout;
import com.sbt.domain.plugin.DomainTypeComplitionProvider;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class NewClassDialog extends DialogWrapper {
    private JPanel main;
    private Project project;
    private JTextField className;
    private JTextField packageName;
    private JTextField tableName;
    private JPanel propertiesPanel;
    private JPanel newPropertyPanel;
    private JTextField newPropertyName;

    private TextFieldWithAutoCompletion<String> newPropertyType;

    private List<PropertyDescription> properties = new ArrayList<>();
    private final JTextField newPropertyColumn;
    private final JCheckBox newPropertyIsId;

    @Nullable
    @Override
    protected JComponent createCenterPanel() {
        return LabeledComponent.create(main, "New class");

    }

    public NewClassDialog(@Nullable Project project) {
        super(project);
        this.project = project;
        setTitle("New class");
        main = new JPanel();
        main.setLayout(new VerticalLayout(0, 0));

        JPanel classPanel = new JPanel();

        className = new JTextField();
        classPanel.add(LabeledComponent.create(className, "class name"));
        packageName = new JTextField();
        classPanel.add(LabeledComponent.create(packageName, "package"));
        tableName = new JTextField();
        classPanel.add(LabeledComponent.create(tableName, "table"));
        JButton create = new JButton();
        create.setText("create");
        create.addActionListener(e -> createClass());
        classPanel.add(create);
        main.add(classPanel);

        propertiesPanel = new JPanel();
        propertiesPanel.setLayout(new VerticalLayout(0, 0));

        main.add(propertiesPanel);

        newPropertyPanel = new JPanel();

        newPropertyName = new JTextField();
        newPropertyPanel.add(LabeledComponent.create(newPropertyName, "property name"));

        newPropertyType = TextFieldWithAutoCompletion.create(project, Collections.EMPTY_LIST, true, "");
        newPropertyType.installProvider(new DomainTypeComplitionProvider());
        newPropertyPanel.add(LabeledComponent.create(newPropertyType, "property type"));

        newPropertyColumn = new JTextField();
        newPropertyPanel.add(LabeledComponent.create(newPropertyColumn, "property column"));

        newPropertyIsId = new JCheckBox();
        newPropertyPanel.add(LabeledComponent.create(newPropertyIsId, "id"));

        JButton addProperty = new JButton();
        addProperty.setText("Add");
        newPropertyPanel.add(addProperty);
        addProperty.addActionListener(e -> addProperty());

        main.add(newPropertyPanel);

        init();
    }

    private void addProperty() {
        PropertyDescription description = new PropertyDescription(newPropertyName.getText(), newPropertyType.getText(),
                newPropertyColumn.getText(), newPropertyIsId.isSelected());
        properties.add(description);
        newPropertyName.setText("");
        newPropertyType.setText("");
        newPropertyColumn.setText("");
        newPropertyIsId.setSelected(false);

        JPanel panel = createPropertyPanel(description);

        propertiesPanel.add(panel);
        propertiesPanel.hide();
        propertiesPanel.show();
    }

    @NotNull
    private JPanel createPropertyPanel(PropertyDescription description) {
        JPanel panel = new JPanel();
        panel.add(new Label(description.getName()));
        panel.add(new Label(description.getType()));
        panel.add(new Label(description.getColumn()));
        panel.add(new Label(description.isId() ? "id" : ""));
        return panel;
    }

    private void createClass() {
        VirtualFile[] directoriesByPackageName =
                PackageIndex.getInstance(project).getDirectoriesByPackageName(packageName.getText(), true);
        PsiDirectory directory = PsiManager.getInstance(project).findDirectory(directoriesByPackageName[0]);
//                JavaDirectoryService.getInstance().
        PsiClass aClass = JavaDirectoryService.getInstance().createClass(directory, className.getText());
        new WriteCommandAction.Simple(project, aClass.getContainingFile()) {
            @Override
            protected void run() throws Throwable {
                writeClass(aClass);
            }
        }.execute();
    }

    private void writeClass(PsiClass aClass) {
        PsiElementFactory factory = JavaPsiFacade.getInstance(aClass.getProject()).getElementFactory();

        JavaCodeStyleManager manager = JavaCodeStyleManager.getInstance(aClass.getProject());

        manager.shortenClassReferences(
                aClass.addBefore(
                        factory.createAnnotationFromText("@javax.persistence.Entity", aClass)
                        , aClass));

        manager.shortenClassReferences(
                aClass.addBefore(
                        factory.createAnnotationFromText(
                                "@javax.persistence.Table(name = \"" + tableName.getText() + "\")", aClass)
                        , aClass));
        for (PropertyDescription description : properties) {

            manager.shortenClassReferences(
                    aClass.add(
                            factory.createFieldFromText(
                                    createFieldText(description), aClass)));
            manager.shortenClassReferences(
                    aClass.add(
                            factory.createMethodFromText(
                                    DomainDialog.createGetterText(description.getName(), description.getType()), aClass)));
            manager.shortenClassReferences(
                    aClass.add(
                            factory.createMethodFromText(
                                    DomainDialog.createSetterText(description.getName(), description.getType()), aClass)));

        }
    }

    @NotNull
    private String createFieldText(PropertyDescription description) {
        return (description.isId() ? "@javax.persistence.Id" : "") +
                (description.getColumn().isEmpty()
                        ? ""
                        : "@javax.persistence.Column(name = \"" + description.getColumn() + "\")") +
                "private " + description.getType() + " " + description.getName() + ";";
    }


    private class PropertyDescription {
        private String name;
        private String type;
        private String column;
        private boolean id;

        public PropertyDescription(String name, String type, String column, boolean id) {
            this.name = name;
            this.type = type;
            this.column = column;
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public String getColumn() {
            return column;
        }

        public boolean isId() {
            return id;
        }
    }

}
