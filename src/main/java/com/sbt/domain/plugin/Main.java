package com.sbt.domain.plugin;

import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) {
        SQLgeneration s = new SQLgeneration();
        //Elephant s = new Elephant();
        s.create(Elephant.class);
        for (Field x : s.getClass().getDeclaredFields()) {
            System.out.println(x.getName() + " " + x.getType());
        }
    }
}
