package com.sbt.domain.plugin;

import com.intellij.codeInsight.completion.CompletionParameters;
import com.intellij.ui.TextFieldWithAutoCompletionListProvider;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.stream.Collectors;

// TextFieldWithAutoCompletion.StringsCompletionProvider

public class DomainTypeComplitionProvider extends TextFieldWithAutoCompletionListProvider<String> {
    public DomainTypeComplitionProvider() {
        super(DomainStorage.getInstance().getAllClassesNames());
    }

    @NotNull
    @Override
    protected String getLookupString(@NotNull String s) {
        return s;
    }

    @NotNull
    @Override
    public Collection<String> getItems(String s, boolean b, CompletionParameters completionParameters) {


        //return super.getItems(s, b, completionParameters);
        return DomainStorage.getInstance().getAllClassesNames().stream().filter(name -> name.contains(s)).collect(
                Collectors.toList());
    }
}
