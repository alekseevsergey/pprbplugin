package com.sbt.domain.plugin;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.util.Date;
import java.util.Map;

public class SQLgeneration{

    public void create(Class inClass) {
        File dir = new File("db");
        dir.mkdir();
        Date d = new Date();
        long idScrypt = d.getTime()/1000;
        StringBuilder sqlQueryCreate = new StringBuilder();
        sqlQueryCreate.append("BEGIN\n\t");
        sqlQueryCreate.append("ora_tools.create_seq('" + inClass.getSimpleName().toUpperCase() + "_SEQ');\n");
        sqlQueryCreate.append("END;\n");
        sqlQueryCreate.append("/\n\n");
        sqlQueryCreate.append("CREATE TABLE " + inClass.getSimpleName().toUpperCase() + "\n");
        sqlQueryCreate.append("(\n\t");
        Map<String, String> convertMap = JavaToSQLvar.convertToSQL(inClass);
        
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(dir + "/" + idScrypt + "_" + inClass.getSimpleName() + "_create.sql"), "utf-8"))) {
            writer.write(sqlQueryCreate.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

//    public void addColumn(Class inClass, String columnName) {
//
//    }
//
//    public void dropColumn(Class inClass, String columnName) {
//
//    }
//
//    public void updateColumn(Class inClass, String columnName) {
//
//    }


}