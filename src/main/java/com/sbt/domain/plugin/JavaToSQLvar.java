package com.sbt.domain.plugin;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class JavaToSQLvar {

    public static Map<String, String> convertToSQL(Class inClass){
        Map<String, String> result = new HashMap();
        Map<String, String> convertMap = new HashMap();
        convertMap.put("java.lang.String", "VARCHAR");
        convertMap.put("int", "int");
        convertMap.put("long", "NUMBER");
        convertMap.put("short", "SMALLINT");
        convertMap.put("float", "FLOAT");
        convertMap.put("double", "DOUBLE");
        convertMap.put("byte", "TINYINT");
        convertMap.put("boolean", "NUMBER");
        convertMap.put("java.lang.Integer", "INTEGER");
        convertMap.put("java.lang.Long", "NUMBER");
        convertMap.put("java.lang.Short", "SMALLINT");
        convertMap.put("java.lang.Float", "FLOAT");
        convertMap.put("java.lang.Double", "DOUBLE");
        convertMap.put("java.lang.byte", "TINYINT");
        convertMap.put("java.lang.Boolean", "NUMBER");
        convertMap.put("java.lang.Character", "VARCHAR");

        for (Field x : inClass.getClass().getDeclaredFields()) {
            result.put(x.getName(), convertMap.get(x.getType()));
        }
        return result;
    }
}
