package com.sbt.domain.plugin;

import com.intellij.psi.PsiElement;
import com.intellij.psi.impl.source.PsiJavaFileImpl;
import org.jetbrains.annotations.NotNull;

public class PsiHelper {
    @NotNull
    static String fetchPackageNameForClass(PsiElement parent) {
        String packageFullName = "";
        if (parent instanceof PsiJavaFileImpl) {
            packageFullName = ((PsiJavaFileImpl) parent).getPackageName();
        } else {
            PsiElement parent1 = parent.getParent();
            if (parent1 instanceof PsiJavaFileImpl)
                packageFullName = ((PsiJavaFileImpl) parent1).getPackageName();
            else
                parent1.getText();
        }
        return packageFullName;
    }
}